//USAGE node fme_deploy.js fmeuser fmetoken filename serverUrl

const fetch = require('node-fetch');
const fs = require('fs');
const async = require('async');

const args = process.argv.slice(2);

const user = args[0];
const fmetoken = args[1];
const filename = args[2];
const serverUrl = args[3];

console.log('User: ' + user);
console.log('FMEToken: ' + fmetoken);
console.log('Project file: ' + filename);
console.log('Server URL: ' + serverUrl);

function checkResponseStatus(res) {
    if(res.ok){
        return res;
    } else {
        throw new Error(`The HTTP status of the reponse: ${res.status} (${res.statusText})`);
    }
}

function apiCall(url,headers,body) {
    fetch(url, {
        method: 'POST',
        headers: headers,
        body: body
    }).then(checkResponseStatus)
        .then(res => res.json())
        .then(json => console.log(json))
        .catch(err => console.log(err));
}

//upload parameters
const payload = fs.createReadStream(__dirname + '/projects/'+ filename, (err,data) => {
    if (err) {
        console.error(err);
        return;
    }
});
const uploadurl = serverUrl + '/fmerest/v3/resources/connections/FME_SHAREDRESOURCE_TEMP/filesys/project?createDirectories=true&overwrite=true&token='+fmetoken;
const uploadheaders = {'Content-Disposition': 'attachment; filename= ' + filename ,
    'Content-Type': 'application/octet-stream',
    'Accept': 'application/json',
    'authorization':'fmetoken token='+fmetoken  };

//import parameters
const importurl = serverUrl +'/fmerest/v3/projects/import/resource?fmetoken='+fmetoken;
const importheaders = { 'Content-Type': 'application/x-www-form-urlencoded','Accept': 'application/json' };
const importbody = 'disableProjectItems=false&importMode=UPDATE&importPackage=%2Fproject%2F'+filename+'&pauseNotifications=true&resourceName=FME_SHAREDRESOURCE_TEMP';

//series calls
async.series([
    callback => {
        apiCall(uploadurl,uploadheaders,payload);
        console.log('Uploaded project file');
        callback();
    },
    callback => {
        setTimeout(apiCall, 3000, importurl,importheaders,importbody);
        console.log('Importing project file');
        callback();
    }
]).catch(err => console.log(err));
