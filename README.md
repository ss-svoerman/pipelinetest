# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Template/test repository for FME projects requiring deployment to FME Cloud.
* Version: Original
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Author and publish FME workspaces to FME Server
* Build your project on FME Server and export to /projects folder
* Set pipeline parameter for the project file name
* NOTE: deployment to UAT and PROD is a manual pipeline step.


### Who do I talk to? ###

* Contact BTS for more information
